package api

import (
	"bitbucket.org/wayne_brush/skunkworks-svc/configuration"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"testing"
)

func TestAPI_initRoutes(t *testing.T) {
	cfg := configuration.InitConfig("", "")

	api := NewAPI(cfg, "../../docs/")
	api.router = mux.NewRouter()
	api.initRoutes(negroni.New())
}
