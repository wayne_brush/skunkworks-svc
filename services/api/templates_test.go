package api

//import (
//	commondm "bitbucket.org/optiisolutions/go-common/datamodels"
//	"bitbucket.org/optiisolutions/go-common/helpers"
//	"bitbucket.org/optiisolutions/skunkworks-svc/models"
//	"bytes"
//	"encoding/json"
//	"fmt"
//	"net/http"
//	"net/url"
//	"strings"
//	"testing"
//
//	"bitbucket.org/optiisolutions/sd-assigner-svc/configuration"
//	"bitbucket.org/optiisolutions/sd-assigner-svc/dao/mock_dao"
//	"github.com/golang/mock/gomock"
//)
//
//var (
//	testTemplateData = datamodels.Template{
//		Id:          1,
//		Name:        "test",
//		Status:      datamodels.TemplateStatusNew,
//		Description: "a long time ago ina galaxy far far away...",
//	}
//)
//
//func TestAPI_CreateTemplate(t *testing.T) {
//	cfg := configuration.InitConfig("", "")
//
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	tests := []struct {
//		name        string
//		template    *datamodels.Template
//		body        string
//		createError error
//		wantStatus  int
//	}{
//		{
//			name:        "creating template good case",
//			template:    &testTemplateData,
//			body:        helpers.HelperJsonMarshallMust(&testTemplateData),
//			createError: nil,
//			wantStatus:  http.StatusOK,
//		},
//		{
//			name:        "bad input data case",
//			template:    nil,
//			body:        "",
//			createError: nil,
//			wantStatus:  http.StatusBadRequest,
//		},
//		{
//			name:        "creating template error case",
//			template:    &testTemplateData,
//			body:        helpers.HelperJsonMarshallMust(&testTemplateData),
//			createError: fmt.Errorf("test"),
//			wantStatus:  http.StatusInternalServerError,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			mdao := mock_dao.NewMockDataAccessObject(ctrl)
//			api := NewAPI(cfg, "../../docs/", mdao)
//
//			mdao.
//				EXPECT().
//				CreateTemplate(gomock.Any()).
//				Return(false, tt.createError).
//				MaxTimes(1)
//
//			helpers.HelperHttpHandlerTest(helpers.HandlerTestData{
//				T:           t,
//				Method:      "POST",
//				URL:         "/api/v1/templates",
//				Body:        strings.NewReader(tt.body),
//				HandlerFunc: api.CreateTemplate,
//				ExpStatus:   tt.wantStatus,
//				BodyCheckFunc: func(body *bytes.Buffer) error {
//					if tt.wantStatus == http.StatusOK {
//						response := body.Bytes()
//						values := make(map[string]interface{})
//						err := json.Unmarshal(response, &values)
//						if err != nil {
//							return fmt.Errorf("bad json returned from unmarshal = %s", err.Error())
//						}
//						if status, ok := values["status"]; !ok || (tt.template != nil && !strings.EqualFold(status.(string), string(tt.template.Status))) {
//							return fmt.Errorf("handler returned unexpected body: %#v, when status expected to be %s, but get %s", values, tt.template.Status, status)
//						}
//					}
//
//					return nil
//				},
//			})
//		})
//	}
//}
//
//func TestAPI_GetTemplate(t *testing.T) {
//	cfg := configuration.InitConfig("", "")
//
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	tests := []struct {
//		name       string
//		id         string
//		template   *datamodels.Template
//		isFound    bool
//		getError   error
//		wantStatus int
//	}{
//		{
//			name:       "finding template good case",
//			id:         "1",
//			template:   &testTemplateData,
//			isFound:    true,
//			getError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name:       "bad id format case",
//			id:         "bad",
//			wantStatus: http.StatusBadRequest,
//		},
//		{
//			name:       "finding template error",
//			id:         "2",
//			template:   nil,
//			isFound:    false,
//			getError:   fmt.Errorf("test error"),
//			wantStatus: http.StatusInternalServerError,
//		},
//		{
//			name:       "template not found",
//			id:         "2",
//			template:   nil,
//			isFound:    false,
//			getError:   nil,
//			wantStatus: http.StatusBadRequest,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			mdao := mock_dao.NewMockDataAccessObject(ctrl)
//			api := NewAPI(cfg, "../../docs/", mdao)
//
//			mdao.
//				EXPECT().
//				GetTemplateById(gomock.Any()).
//				Return(tt.template, tt.isFound, tt.getError).
//				MaxTimes(1)
//
//			helpers.HelperHttpHandlerTest(helpers.HandlerTestData{
//				T:           t,
//				Method:      "GET",
//				URLMask:     "/api/v1/templates/{id}",
//				URL:         "/api/v1/templates/" + tt.id,
//				Body:        nil,
//				HandlerFunc: api.GetTemplate,
//				ExpStatus:   tt.wantStatus,
//				BodyCheckFunc: func(body *bytes.Buffer) error {
//					if tt.wantStatus == http.StatusOK {
//						response := body.Bytes()
//						values := make(map[string]interface{})
//						err := json.Unmarshal(response, &values)
//						if err != nil {
//							return fmt.Errorf("bad json returned from unmarshal = %s", err.Error())
//						}
//						if id, ok := values["templateId"]; !ok || !strings.EqualFold(fmt.Sprintf("%d", int64(id.(float64))), tt.id) {
//							return fmt.Errorf("handler returned unexpected body: %#v", values)
//						}
//					}
//
//					return nil
//				},
//			})
//		})
//	}
//}
//
//func TestAPI_ListTemplates(t *testing.T) {
//	cfg := configuration.InitConfig("", "")
//
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	tests := []struct {
//		name       string
//		filters    url.Values
//		templates  []datamodels.Template
//		total      int
//		hasMore    bool
//		getError   error
//		wantStatus int
//	}{
//		{
//			name: "finding templates list good case",
//			filters: url.Values{
//				"name": []string{testTemplateData.Name},
//			},
//			templates:  []datamodels.Template{testTemplateData},
//			total:      1,
//			hasMore:    false,
//			getError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name: "unknown filters case",
//			filters: url.Values{
//				"unknown": []string{"test"},
//			},
//			templates:  []datamodels.Template{testTemplateData},
//			total:      0,
//			hasMore:    false,
//			getError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name: "finding templates fails case",
//			filters: url.Values{
//				"name": []string{testTemplateData.Name},
//			},
//			templates:  []datamodels.Template{},
//			total:      0,
//			hasMore:    false,
//			getError:   fmt.Errorf("test"),
//			wantStatus: http.StatusInternalServerError,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			mdao := mock_dao.NewMockDataAccessObject(ctrl)
//			api := NewAPI(cfg, "../../docs/", mdao)
//
//			mdao.
//				EXPECT().
//				ListTemplates(gomock.Any()).
//				Return(tt.templates, tt.total, tt.hasMore, tt.getError).
//				MaxTimes(1)
//
//			helpers.HelperHttpHandlerTest(helpers.HandlerTestData{
//				T:           t,
//				Method:      "GET",
//				URL:         "/api/v1/templates",
//				Query:       tt.filters,
//				HandlerFunc: api.ListTemplates,
//				ExpStatus:   tt.wantStatus,
//				BodyCheckFunc: func(body *bytes.Buffer) error {
//					if tt.wantStatus == http.StatusOK {
//						response := body.Bytes()
//						var list commondm.List
//						err := json.Unmarshal(response, &list)
//						if err != nil {
//							return fmt.Errorf("bad json returned from unmarshal = %s", err.Error())
//						}
//						if len(list.Edges) != len(tt.templates) {
//							return fmt.Errorf("handler returned unexpected body list want len %d got len %d: ", len(list.Edges), len(tt.templates))
//						}
//						if t, ok := list.Edges[0].Node.(map[string]interface{}); !ok || (len(tt.templates) > 0 && (t["templateId"] == nil || int64(t["templateId"].(float64)) != tt.templates[0].Id)) {
//							return fmt.Errorf("handler returned unexpected body want %v got %v: of %#v", t, tt.templates[0], list)
//						}
//					}
//
//					return nil
//				},
//			})
//		})
//	}
//}
//
//func TestAPI_UpdateTemplate(t *testing.T) {
//	cfg := configuration.InitConfig("", "")
//
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//	newStatus := string(datamodels.TemplateStatusActive)
//	tests := []struct {
//		name       string
//		id         string
//		template   *datamodels.Template
//		isFound    bool
//		getError   error
//		body       string
//		isFoundUpd bool
//		updError   error
//		wantStatus int
//	}{
//		{
//			name:     "updating template good case",
//			id:       "1",
//			template: &testTemplateData,
//			isFound:  true,
//			getError: nil,
//			body: helpers.HelperJsonMarshallMust(datamodels.TemplateUpdate{
//				Status: &newStatus,
//			}),
//			isFoundUpd: true,
//			updError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name:       "bad id format case",
//			id:         "bad",
//			wantStatus: http.StatusBadRequest,
//		},
//		{
//			name:       "finding template error",
//			id:         "2",
//			template:   nil,
//			isFound:    false,
//			getError:   fmt.Errorf("test error"),
//			wantStatus: http.StatusInternalServerError,
//		},
//		{
//			name:       "template not found",
//			id:         "2",
//			template:   nil,
//			isFound:    false,
//			getError:   nil,
//			wantStatus: http.StatusBadRequest,
//		},
//		{
//			name:       "bad update data case",
//			id:         "1",
//			template:   &testTemplateData,
//			isFound:    true,
//			getError:   nil,
//			body:       "",
//			isFoundUpd: false,
//			updError:   nil,
//			wantStatus: http.StatusBadRequest,
//		},
//		{
//			name:       "nothing to update case",
//			id:         "1",
//			template:   &testTemplateData,
//			isFound:    true,
//			getError:   nil,
//			body:       helpers.HelperJsonMarshallMust(struct{ f *string }{f: &newStatus}),
//			isFoundUpd: false,
//			updError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name:     "update error case",
//			id:       "1",
//			template: &testTemplateData,
//			isFound:  true,
//			getError: nil,
//			body: helpers.HelperJsonMarshallMust(datamodels.TemplateUpdate{
//				Status: &newStatus,
//			}),
//			isFoundUpd: true,
//			updError:   fmt.Errorf("test"),
//			wantStatus: http.StatusInternalServerError,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			mdao := mock_dao.NewMockDataAccessObject(ctrl)
//			api := NewAPI(cfg, "../../docs/", mdao)
//
//			mdao.
//				EXPECT().
//				GetTemplateById(gomock.Any()).
//				Return(tt.template, tt.isFound, tt.getError).
//				MaxTimes(1)
//
//			mdao.
//				EXPECT().
//				UpdateTemplate(gomock.Any()).
//				Return(tt.updError).
//				MaxTimes(1)
//
//			helpers.HelperHttpHandlerTest(helpers.HandlerTestData{
//				T:           t,
//				Method:      "PUT",
//				URLMask:     "/api/v1/templates/{id}",
//				URL:         "/api/v1/templates/" + tt.id,
//				Body:        strings.NewReader(tt.body),
//				HandlerFunc: api.UpdateTemplate,
//				ExpStatus:   tt.wantStatus,
//				BodyCheckFunc: func(body *bytes.Buffer) error {
//					if tt.wantStatus == http.StatusOK {
//						response := body.Bytes()
//						values := make(map[string]interface{})
//						err := json.Unmarshal(response, &values)
//						if err != nil {
//							return fmt.Errorf("bad json returned from unmarshal = %s", err.Error())
//						}
//						if id, ok := values["templateId"]; !ok || !strings.EqualFold(fmt.Sprintf("%d", int64(id.(float64))), tt.id) {
//							return fmt.Errorf("%#v", values)
//						}
//					}
//
//					return nil
//				},
//			})
//		})
//	}
//}
//
//func TestAPI_DeleteTemplate(t *testing.T) {
//	cfg := configuration.InitConfig("", "")
//
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	tests := []struct {
//		name       string
//		id         string
//		isFound    bool
//		delError   error
//		wantStatus int
//	}{
//		{
//			name:       "deleting template good case",
//			id:         "1",
//			isFound:    true,
//			delError:   nil,
//			wantStatus: http.StatusOK,
//		},
//		{
//			name:       "bad id format case",
//			id:         "bad",
//			wantStatus: http.StatusBadRequest,
//		},
//		{
//			name:       "finding template error",
//			id:         "2",
//			isFound:    false,
//			delError:   fmt.Errorf("test error"),
//			wantStatus: http.StatusInternalServerError,
//		},
//		{
//			name:       "template not found",
//			id:         "2",
//			isFound:    false,
//			delError:   nil,
//			wantStatus: http.StatusBadRequest,
//		},
//	}
//
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			mdao := mock_dao.NewMockDataAccessObject(ctrl)
//			api := NewAPI(cfg, "../../docs/", mdao)
//
//			mdao.
//				EXPECT().
//				DeleteTemplateById(gomock.Any()).
//				Return(tt.isFound, tt.delError).
//				MaxTimes(1)
//
//			helpers.HelperHttpHandlerTest(helpers.HandlerTestData{
//				T:           t,
//				Method:      "DELETE",
//				URLMask:     "/api/v1/templates/{id}",
//				URL:         "/api/v1/templates/" + tt.id,
//				Body:        nil,
//				HandlerFunc: api.DeleteTemplate,
//				ExpStatus:   tt.wantStatus,
//				BodyCheckFunc: func(body *bytes.Buffer) error {
//					if tt.wantStatus == http.StatusOK {
//						response := body.Bytes()
//						values := make(map[string]interface{})
//						err := json.Unmarshal(response, &values)
//						if err != nil {
//							return fmt.Errorf("bad json returned from unmarshal = %s", err.Error())
//						}
//						if id, ok := values["id"]; !ok || !strings.EqualFold(fmt.Sprintf("%d", int64(id.(float64))), tt.id) {
//							return fmt.Errorf("handler returned unexpected body: %#v", values)
//						}
//					}
//
//					return nil
//				},
//			})
//		})
//	}
//}
