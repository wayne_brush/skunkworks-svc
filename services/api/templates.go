package api

// swagger:operation POST /templates template create
// ---
// summary: Creates the new template
// description: returns template model if created
// parameters:
// - name: Accept
//   in: header
//   description: standard "Accept" header values
//   type: string
//   required: true
// produces:
//   - application/json
// responses:
//   '200':
//     description: "OK; returns empty page or json structure"
//func (api *API) CreateTemplate(w http.ResponseWriter, r *http.Request) {
//	logrus.Trace("requested CreateTemplate")
//
//	var newTemplate models.Template
//	decoder := json.NewDecoder(r.Body)
//	err := decoder.Decode(&newTemplate)
//	if err != nil {
//		logrus.Warnf("wrong input data provided: %s", err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrBadParam, "body"))
//		return
//	}
//
//	//remove filled fields if provided
//	newTemplate.Id = 0
//
//	isDuplicate, err := api.dao.CreateTemplate(&newTemplate)
//	if err != nil {
//		logrus.Errorf("Template creation error: %s", err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//	if isDuplicate {
//		logrus.Warnf("Template already exist: %s", newTemplate.Name)
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrAlreadyExists, newTemplate.Name))
//		return
//	}
//
//	httphelper.Json(w, newTemplate)
//	logrus.Trace("finished CreateTemplate")
//	return
//}

// swagger:operation GET /templates/{id} template get by id
// ---
// summary: Gets single Template by ID
// description: returns template model if exists
// parameters:
// - name: Accept
//   in: header
//   description: standard "Accept" header values
//   type: string
//   required: true
// produces:
//   - application/json
// responses:
//   '200':
//     description: "OK; returns empty page or json structure"
//func (api *API) GetTemplate(w http.ResponseWriter, r *http.Request) {
//	logrus.Trace("requested GetTemplate")
//
//	ids := mux.Vars(r)["id"]
//	id, err := strconv.ParseInt(ids, 10, 64)
//	if err != nil {
//		logrus.Warnf("wrong template id %s provided by user: %s", ids, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrBadParam, ids))
//		return
//	}
//
//	template, found, err := api.dao.GetTemplateById(id)
//	if err != nil {
//		logrus.Errorf("Template id %d select error: %s", id, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//	if !found || template == nil {
//		logrus.Warnf("No such template by id %d was found", id)
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrNotFound, ids))
//		return
//	}
//
//	httphelper.Json(w, template)
//	logrus.Trace("finished GetTemplate")
//	return
//}

// swagger:operation GET /templates templates listTemplates
// ---
//description: returns templates list
//produces:
//- application/json
//tags:
//- templates
//summary: Return list of templates
//operationId: listTemplates
//parameters:
//- in: query
//  name: first
//  description: how many objects must to return
//  schema:
//    type: integer
//- in: query
//  name: after
//  description: cursor of object that must to be returned
//  schema:
//    type: string
//- in: query
//  name: orderBy
//  description: on which field results must to be ordered
//  schema:
//    type: string
//- in: query
//  name: filters
//  description: 'filters on fields, which can be provided in format described here:
//    https://godoc.org/github.com/go-pg/pg/urlvalues#Filter'
//  schema:
//    type: object
//responses:
//  '200':
//    description: OK
//    schema:
//      type: array
//      items:
//        "$ref": "#/definitions/GlobalList"
//func (api *API) ListTemplates(w http.ResponseWriter, r *http.Request) {
//	logrus.Trace("requested ListTemplates")
//
//	templates, total, hasNext, err := api.dao.ListTemplates(r.URL.Query())
//	if err != nil {
//		logrus.Errorf("Departments list select error: %s", err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//
//	var list commondm.List
//	list.TotalCount = total
//	list.Edges = make([]commondm.Edge, 0)
//	for i := range templates {
//		list.Edges = append(list.Edges, commondm.Edge{
//			Node:   templates[i],
//			Cursor: db.EncodeIdToCursor(templates[i].Id),
//		})
//	}
//
//	list.PageInfo.HasNextPage = hasNext
//	if len(templates) > 0 {
//		list.PageInfo.EndCursor = db.EncodeIdToCursor(templates[len(templates)-1].Id)
//	}
//
//	httphelper.Json(w, list)
//	logrus.Trace("finished ListTemplates")
//	return
//}

// swagger:operation POST /templates/{id} template update by id
// ---
// summary: Updates single Template by ID
// description: returns template model if updated
// parameters:
// - name: Accept
//   in: header
//   description: standard "Accept" header values
//   type: string
//   required: true
// produces:
//   - application/json
// responses:
//   '200':
//     description: "OK; returns empty page or json structure"
//func (api *API) UpdateTemplate(w http.ResponseWriter, r *http.Request) {
//	logrus.Trace("requested UpdateTemplate")
//
//	ids := mux.Vars(r)["id"]
//	id, err := strconv.ParseInt(ids, 10, 64)
//	if err != nil {
//		logrus.Warnf("wrong template id %s provided by user: %s", ids, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrBadParam, ids))
//		return
//	}
//
//	template, found, err := api.dao.GetTemplateById(id)
//	if err != nil {
//		logrus.Errorf("Template id %d select error: %s", id, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//	if !found || template == nil {
//		logrus.Warnf("No such template by id %d was found", id)
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrNotFound, ids))
//		return
//	}
//
//	var updTemplateData models.TemplateUpdate
//	decoder := json.NewDecoder(r.Body)
//	err = decoder.Decode(&updTemplateData)
//	if err != nil {
//		logrus.Warnf("wrong input data provided: %s", err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrBadParam, "body"))
//		return
//	}
//
//	isFoundUpd, err := helpers.NullableFieldsToStruct(updTemplateData, template)
//	if err != nil {
//		logrus.Warnf("cannot fill updateable fields: %s", err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//	if !isFoundUpd {
//		logrus.Infof("nothing to update for template %d", template.Id)
//		httphelper.Json(w, template)
//		return
//	}
//
//	err = api.dao.UpdateTemplate(template)
//	if err != nil {
//		logrus.Errorf("template id %d update error: %s", id, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//
//	httphelper.Json(w, template)
//	logrus.Trace("finished UpdateTemplate")
//	return
//}

// swagger:operation DELETE /templates/{id} template get by id
// ---
// summary: Removes single Template by ID
// description: returns template id if removed
// parameters:
// - name: Accept
//   in: header
//   description: standard "Accept" header values
//   type: string
//   required: true
// produces:
//   - application/json
// responses:
//   '200':
//     description: "OK; returns empty page or json structure with removed model id"
//func (api *API) DeleteTemplate(w http.ResponseWriter, r *http.Request) {
//	logrus.Trace("requested DeleteTemplate")
//
//	ids := mux.Vars(r)["id"]
//	id, err := strconv.ParseInt(ids, 10, 64)
//	if err != nil {
//		logrus.Warnf("wrong template id %s provided: %s", ids, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrBadParam, ids))
//		return
//	}
//
//	template := &models.Template{Id: id}
//
//	found, err := api.dao.DeleteTemplateById(id)
//	if err != nil {
//		logrus.Errorf("Template id %d delete error: %s", id, err.Error())
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrService))
//		return
//	}
//	if !found {
//		logrus.Warnf("No such template by id %d was found", id)
//		httphelper.JsonError(w, errorhandler.NewError(errorhandler.ErrNotFound, ids))
//		return
//	}
//
//	httphelper.Json(w, map[string]interface{}{"id": template.Id})
//	logrus.Trace("finished DeleteTemplate")
//	return
//}
