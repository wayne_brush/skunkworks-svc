package api

import (
	"bitbucket.org/wayne_brush/skunkworks-svc/configuration"
	"github.com/urfave/negroni"
)

//const (
//	ApiEmployeeUserPath = "/employee/user"
//)

func (api *API) initRoutes(wrapper *negroni.Negroni) {
	api.HandleActions(wrapper, configuration.APIBasePath, []Route{
		{
			Name:        "Info",
			Method:      "GET",
			Pattern:     "/info",
			HandlerFunc: api.HandleInfo,
			Middleware:  nil,
		},
		{
			Name:        "Ping",
			Method:      "GET",
			Pattern:     "/ping",
			HandlerFunc: api.HandlePing,
			Middleware:  nil,
		},
	})
	//api.HandleActions(wrapper, configuration.APIBasePath+configuration.APIVersion, []Route{
	//	//  application specific
	//	{
	//		Name:        "Create Template",
	//		Method:      "POST",
	//		Pattern:     "/templates",
	//		HandlerFunc: api.CreateTemplate,
	//		Middleware:  nil,
	//	},
	//	{
	//		Name:        "Get Template",
	//		Method:      "GET",
	//		Pattern:     "/templates/{id}",
	//		HandlerFunc: api.GetTemplate,
	//		Middleware:  nil,
	//	},
	//	{
	//		Name:        "List Templates",
	//		Method:      "GET",
	//		Pattern:     "/templates",
	//		HandlerFunc: api.ListTemplates,
	//		Middleware:  nil,
	//	},
	//	{
	//		Name:        "Update Template",
	//		Method:      "PUT",
	//		Pattern:     "/templates/{id}",
	//		HandlerFunc: api.UpdateTemplate,
	//		Middleware:  nil,
	//	},
	//	{
	//		Name:        "Remove Template",
	//		Method:      "DELETE",
	//		Pattern:     "/templates/{id}",
	//		HandlerFunc: api.DeleteTemplate,
	//		Middleware:  nil,
	//	},
	//})
}
