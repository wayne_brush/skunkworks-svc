// Package classification Skunkworks Service
//
// the purpose of this service is to
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http, https
//     Host: localhost
//     BasePath: /api
//     Version: 0.0.1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     security:
//     - optii_apikey:
//	   - optii_oauth2:
//	     read_scope
//
// 	   securityDefinitions:
//       optii_apikey:
//         type: apiKey
//         name: KEY
//         in: header
//       optii_oauth2:
//         type: oauth2
//         description: example
//         flow: accessCode
//         authorizationUrl: 'https://localhost/oauth2/auth'
//         tokenUrl: 'https://localhost/oauth2/token'
//         scopes:
//           read_scope: description here
//           write_scope: description here
//
// swagger:meta
package main

//go:generate swagger generate spec -m -o ./docs/swagger.json

/*
for more information about generating swagger.json from comments, see:
	https://www.ribice.ba/swagger-golang/
*/
import (
	"bitbucket.org/optiisolutions/go-common/helpers"
	"bitbucket.org/wayne_brush/skunkworks-svc/configuration"
	"bitbucket.org/wayne_brush/skunkworks-svc/services/api"
	"bitbucket.org/wayne_brush/skunkworks-svc/services/daemons"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"time"
)

var (
	commit  string
	builtAt string
)

func main() {
	//  perform initialization here; MUST be first to properly set up env vars!
	cfg := configuration.InitConfig(commit, builtAt)

	err := cfg.ConfigureLogger()
	if err != nil {
		logrus.Fatalf("Cannot ConfigureLogger: %s", err.Error())
	}

	logrus.Info("------------------------------")
	logrus.Info("Starting "+configuration.ServiceName)
	logrus.Info("Version:", cfg.Version, "; Build Date:", cfg.BuiltAt)
	logrus.Info("------------------------------")

	//connect to db
	//d, err := postgres.NewPgDao(cfg)
	//if err != nil {
	//	logrus.Fatalf("Cannot init DB: %s", err.Error())
	//}
	//defer d.Close()

	//  init necessary processing routines (modules)
	//  init REST server
	//apiModule := api.NewAPI(cfg, "./docs/", d)
	apiModule := api.NewAPI(cfg, "./docs/")

	// init daemons
	daemonsModule := daemons.NewDaemons(cfg)

	RunModules(apiModule, daemonsModule)
}

func RunModules(modules ...helpers.Module) {
	if len(modules) > 0 {
		for _, m := range modules {
			logrus.Infof("Starting module %s", m.Title())
			go m.Run()
		}

		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt)

		select {
		case <-interrupt:
			fmt.Println() //is used to embellish the output after ^C
			for _, m := range modules {
				logrus.Warnf("Stopping module %s", m.Title())
				ctx, _ := context.WithTimeout(context.Background(), configuration.GracefulStopTimeoutSec*time.Second)
				err := m.GracefulStop(ctx)
				if err != nil {
					logrus.Errorf("can't stop the module %s: %s", m.Title(), err.Error())
				}
			}
		} //lock execution
	}
}
