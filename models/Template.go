package models

//import (
//	"fmt"
//	"strings"
//	"time"
//)
//
//type TemplateStatus string
//
//const (
//	TemplateStatusNew     TemplateStatus = "new"
//	TemplateStatusActive  TemplateStatus = "active"
//	TemplateStatusRemoved TemplateStatus = "removed"
//)
//
//func (status TemplateStatus) IsValid() bool {
//	s := strings.ToLower(string(status))
//
//	switch TemplateStatus(s) {
//	case TemplateStatusNew,
//		TemplateStatusActive,
//		TemplateStatusRemoved:
//		return true
//	default:
//		return false
//	}
//}
//
//type (
//	Template struct {
//		Id          int64          `json:"templateId" sql:"id"`
//		Name        string         `json:"name" sql:"name"`
//		Status      TemplateStatus `json:"status" sql:"status"`
//		Description string         `json:"description" sql:"description"`
//		Self        string         `json:"self" sql:"-"`
//
//		CreatedAt  time.Time  `json:"createdAt" sql:"created_at,default:now()"`
//		UpdatedAt  *time.Time `json:"updatedAt,omitempty" sql:"updated_at"`
//		ArchivedAt *time.Time `json:"archivedAt,omitempty" sql:"archived_at"`
//	}
//
//	TemplateUpdate struct {
//		Status      *string `json:"status,omitempty"`
//		Description *string `json:"description,omitempty"`
//	}
//)
//
//func (t Template) String() string {
//	return fmt.Sprintf("Template<%d %s>", t.Id, t.Name)
//}
//
//func (t Template) Validate() error {
//	if !t.Status.IsValid() {
//		return fmt.Errorf("bad status %s provided", t.Status)
//	}
//
//	// TODO: add here some expected validation rules
//
//	return nil
//}
