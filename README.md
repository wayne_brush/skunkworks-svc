# skunkworks-svc
This service will 

# Development Notes

## Unit Testing
Unit tests have been written for the packages in this service. Optii Solutions goal is 80% coverage for unit tests in new development.

## Developer Notes
This should be modified on a per project basis. I would expect this to be general notes from one developer to the next developer who is tasked with looking at this service.

## Required Packages
Any package (or library) that this service uses that requires a "go get" to use MUST be listed here. Any external package that this service uses should be listed here. 

### Swagger
go get -u github.com/go-swagger/go-swagger/cmd/swagger

### Gorilla
go get -u github.com/gorilla/mux
go get -u github.com/gorilla/handlers

### logrus
go get -u github.com/sirupsen/logrus

### go-pg
go get -u github.com/go-pg/pg/v9
go get -u github.com/go-pg/migrations

# Deployment
Need to document (and/or update) our deployment process here

## Process
 - Pull latest version of the service
 - Verify last deployed version (in the /api/help/ swagger endpoint)
 - Bump the API version for the release
 - Update vendored dependencies
 - Run "go generate" and do a test build locally
 - If successful, check in to github, develop branch
 - Once built and deployed, verify operation (including API version) in dev space
 - In bitbucket, perform PR and merge to master branch
 - Once built and deployed, verify operation (including API version) in QA space
 - In bitbucket, select "Create Release" and enter the release information
 - Once built and deployed, verify operation (including API version) in production

# Services
Every microservice support integrated modules system. Every module located at services directory and must to
implement the *Module* interface.
Every service must to take an arguments for dependency injection 
(like DB connection or config singleton) on New-like constructor func.
When module must run at global service initialisation process, put an instance of your module to 
run list at the params of *RunModules* func call. 

## API module
    the /api directory contains an auto-generated swagger document for the template service.
    there is also code that runs a webdave server on port 8080 with the swagger ui served at
    http://localhost:8080/api/help/index.html
or 
    http://localhost:8080/api/help/

### ToDo
 - Increase unit test coverage in the server package
 
## Daemons module
This is optional module located in template service for example. Daemons module is for periodically 
running background tasks. For example, when you need to expire some records after
some time, it is a good idea to run periodical checking function for records expiration.

Every daemon must to implement *Daemon* interface and must be located inside services/daemons directory.
When daemon must run at global service initialisation process, put an instance of your daemon to 
run list of workers at the *NewDaemons* func. 
