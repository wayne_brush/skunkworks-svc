package configuration

const (
	GracefulStopTimeoutSec   = 5
	ExampleTickerIntervalSec = 30

	ServiceName = "skunkworks-svc"

	APIBasePath = "/api"
	APIVersion  = "/v1"
)
