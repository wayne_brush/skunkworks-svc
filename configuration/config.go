package configuration

import (
	"bitbucket.org/optiisolutions/go-common/config"
	"bitbucket.org/optiisolutions/go-common/messaging"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"
)

type (
	Config struct {
		config.ServiceParams
		config.GCP
	}
)

var cfg Config

func InitConfig(commit, builtAt string) *Config {
	//  load space .env variables first if available
	filename := "./files/.env"
	if _, err := os.Stat(filename); err == nil {
		_ = godotenv.Load(filename)
	}

	cfg := &Config{
		ServiceParams: config.ServiceParams{Environment: "local",
			Host:     "",
			Port:     "8080",
			LogLevel: "debug",
		},
	}

	err := cfg.LoadEnvVariables(cfg, commit, builtAt)
	if err != nil {
		logrus.Fatalf("cannot load config: %s", err.Error())
	}

	cfg.GCP.ServicePubTopic,_ = messaging.PubSubCmdTopic.GetTopicName(cfg.Environment)

	err = cfg.ConfigureLogger()
	if err != nil {
		logrus.Fatalf("cannot ConfigureLogger: %s", err.Error())
	}

	SetCurrentCfg(*cfg)

	return cfg
}

func GetCurrentCfg() Config {
	return cfg
}

func SetCurrentCfg(c Config) {
	cfg = c
}
